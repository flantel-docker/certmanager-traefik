FROM alpine:3.11
RUN apk --no-cache add ca-certificates tzdata monit openrc
RUN set -ex; \
	wget --quiet -O /tmp/traefik.tar.gz "https://github.com/traefik/traefik/releases/download/v2.8.3/traefik_v2.8.3_linux_amd64.tar.gz"; \
	tar xzvf /tmp/traefik.tar.gz -C /usr/local/bin traefik; \
	rm -f /tmp/traefik.tar.gz; \
	chmod +x /usr/local/bin/traefik && \
    wget --quiet -O /tmp/traefik-certs-dumper.tgz  "https://github.com/ldez/traefik-certs-dumper/releases/download/v2.8.1/traefik-certs-dumper_v2.8.1_linux_amd64.tar.gz" && \
    tar xzvf /tmp/traefik-certs-dumper.tgz && mv traefik-certs-dumper /usr/local/bin/ && chmod +x /usr/local/bin/traefik-certs-dumper
COPY makecerts.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/makecerts.sh
COPY entrypoint.sh /
COPY monitrc /etc/monitrc
EXPOSE 80
ENTRYPOINT ["/entrypoint.sh"]
CMD ["certmanager"]

# Metadata
LABEL org.opencontainers.image.vendor="Traefik Labs" \
	org.opencontainers.image.url="https://traefik.io" \
	org.opencontainers.image.title="Traefik" \
	org.opencontainers.image.description="A modern reverse-proxy" \
	org.opencontainers.image.version="v2.8.3" \
	org.opencontainers.image.documentation="https://docs.traefik.io"
