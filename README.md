# Certmanager for Traefik

This is my solution to Traefik 2.x only supporting clustered certificates in Enterprise version.

Essentially, this is a secondary traefik instance that does nothing except handle certificates. Traefik is configured to pass
/.well-known/acme-challenge/ on port 9999 back to this certmanager, which handles the acme, if using http auth for Letsencrypt. 

The example nomad file has certtmanager set up to use dns auth with Route53, but you can quite easily swap that out with any other 
supported resolver. There is no alteration to the traefik binary itself, so the standard traefik docs still apply. 

The way it works is that we have one certmanager-traefik running in our cluster, with a shared storage volume. certmanager-traefik 
manages the certs in the same way as traefik, saving the certs in the usual manner. There is an additional monit (https://www.mmonit.com) 
daemon which runs in the container and watches the acme.json file for changes. When a change occurrs monit runs /usr/local/bin/makecerts.sh 
which uses traefik-certs-dumper (https://github.com/ldez/traefik-certs-dumper) to parse the acme.json and creates a dynamic.toml file. That's it.

The front-end traefik containers mount the shared volume containing dynamic.toml in the correct place, so the frontend trtaefiks will dynamically 
reload this file, thus picking up any new or renewed certificates.

When creating a new service which requires a certificate, you simply use the "certmanager" prefix instead of "traefik" to tell certmanagert to handle the certs. Example:

```
"certmanager.http.routers.certmanager-admin.rule=Host(`certman-admin.example.com`)",
"certmanager.http.routers.certmanager-admin.tls.certresolver=letsencrypt",
"certmanager.http.routers.certmanager-admin.tls=true",
"certmanager.enable=true"
```

So in the above, we are asking certmanager to obtain a cert for certman-admin.example.com using letsencrypt.

The benefit of all of this is you can have many front-end OSS Traefik instances as you need, and certificates are available to them all, 
without the expense of Enterprise. 

See the certmanager.nomad file for clues on how to use this in Docker or K8S. 


The Docker image is available at registry.gitlab.com/flantel-docker/certmanager-traefik

Enjoy

