job "certmanager" {
  region      = "global"
  datacenters = ["do-ams"]
  type        = "service"

  group "certmanager" {
    count = 1
        network {
          port "certbot" {
          to = 9999
          }

          port "http" {
          to = 80
          }

          port "api" {
            to = 8082
          }
        }
    volume "data" {
      type = "host"
      source = "traefik_data"
      read_only = false
    }

    task "certmanager" {
      driver = "docker"
      volume_mount {
        volume      = "data"
        destination = "/data"
      }
      config {
        image        = "flantel/certmanager-traefik:v2.8.3"
        ports = ["certbot", "api"]

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
        ]
      }
      env {
        AWS_ACCESS_KEY_ID = "<YOUR_AWS_ID>"
        AWS_SECRET_ACCESS_KEY = "<YOUR_AWS_KEY"
        AWS_REGION = "eu-west-1"
      }

      template {
        data = <<EOF
[entryPoints]
    [entryPoints.certbot]
    address = ":9999"
    [entryPoints.http]
    address = ":80"
    [entryPoints.traefik]
    address = ":8082"

[certificatesResolvers.letsencrypt.acme]
  email = "hostmaster@example.com"
  caServer = "https://acme-v02.api.letsencrypt.org/directory"
  storage = "/data/acme_volume/acme.json"
  [certificatesResolvers.letsencrypt.acme.dnsChallenge]
    provider = "route53"
    delayBeforeCheck = 0
[log]
  level = "DEBUG"

[api]
    dashboard = true
    insecure  = true

# Enable Consul Catalog configuration backend.
[providers.consulCatalog]
    prefix           = "certmanager"
    exposedByDefault = false
    connectAware = true

    [providers.consulCatalog.endpoint]
      address = "172.17.0.1:8500"
      scheme  = "http"
      token   = "<YOUR CONSUL TOKEN>"
EOF

        destination = "local/traefik.toml"
      }

      resources {
        cpu    = 100
        memory = 128

      }

      service {
        name = "certmanager"
        port = "certbot"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.certmanager.rule=PathPrefix(`/.well-known/acme-challenge/`)"
    ]
      }
    service {
      name = "certmanager-admin"
      port = "api"
    tags = [
        "traefik.enable=true",
        "traefik.http.routers.certmanager-admin.rule=Host(`certman-admin.example.com`)",
        "traefik.http.routers.certmanager-admin.tls=true",
         "traefik.http.routers.certmanager-admin.middlewares=simpleAuth@file",
        "certmanager.http.routers.certmanager-admin.rule=Host(`certman-admin.example.com`)",
        "certmanager.http.routers.certmanager-admin.tls.certresolver=letsencrypt",
        "certmanager.http.routers.certmanager-admin.tls=true",
        "certmanager.enable=true"
        ]

    }
    }
  }
}

