#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    set -- traefik "$@"
fi

# if our command is a valid Traefik subcommand, let's invoke it through Traefik instead
# (this allows for "docker run traefik version", etc)
if [ "$1" = "certmanager" ]
then
     monit -d 10 -c /etc/monitrc
     /usr/local/bin/traefik --configfile /etc/traefik/traefik.toml
else
    echo "= '$1' is not a Traefik command: assuming shell execution." 1>&2
    exec "$@"
fi


