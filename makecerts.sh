#!/bin/sh
#
# This script reads in the certmanager-generated acme.json and splits it out into certs and keys
# and generates dynamic.toml config file which gets picked up by the main Traefik proxies.
#

CONF_DIR=/data/traefik/config
ACME_DIR=/data/acme_volume
DUMP_DIR=${ACME_DIR}/dump

rm -rf ${DUMP_DIR}/{certs,private}
cd $ACME_DIR && /usr/local/bin/traefik-certs-dumper file --version v2

[ "$(ls -A ${DUMP_DIR}/certs/)" ] && for F in ${DUMP_DIR}/certs/*.crt
do
    host=`basename $F .crt`
    cert=${DUMP_DIR}/certs/${host}.crt
    key=${DUMP_DIR}/private/${host}.key
    cat << EOF
    [[tls.certificates]] # $host cert
      certFile = "$cert"
      keyFile = "$key"
EOF
done > ${CONF_DIR}/dynamic.toml
